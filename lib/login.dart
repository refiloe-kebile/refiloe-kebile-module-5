import 'package:flutter/material.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter_app/homepage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();

  String email = '';

  String password = '';

  final passwordController = TextEditingController();

  final emailController = TextEditingController();

  userLogin() async {
    try {
      TextInput.finishAutofillContext(shouldSave: true);
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Home()));
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('Invalid email address!');
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Invalid email address!'),
          ),
        );
      } else if (e.code == 'wrong-password') {
        print('Incorrect Password!');
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Incorrect Password!'),
          ),
        );
      }
      throw {e.code, e.message};
    }
  }

  @override
  void dispose() {
    passwordController.dispose();
    emailController.dispose();
    super.dispose();
  }

  User? currentUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error Occurred, Please try again!'),
            );
          } else if (snapshot.error != null) {
            print(snapshot.error.toString());
            return Text(snapshot.error.toString());
          } else if (snapshot.hasData) {
            context.read<ServiceProvider>().getUserData();

            return Home();
          } else {
            return Form(
              key: _formKey,
              child: formContent(
                context,
                AutofillGroup(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      formTopTitle(context, 'Login'),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 50),
                        child: formEmailTextField(emailController),
                      ),
                      Container(
                        height: 95,
                        margin: EdgeInsets.only(left: 50, right: 50, top: 10),
                        child: Stack(
                          children: [
                            FormPassTextField(passwordController),
                            Positioned(
                                bottom: 0,
                                right: 0,
                                child: TextButton(
                                  style: TextButton.styleFrom(
                                    fixedSize: Size(130, 5),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ForgotPassword()));
                                  },
                                  child: Text(
                                    'Forgetten your password?',
                                    style: TextStyle(
                                        color: Colors.black54, fontSize: 13),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      formActionButton(
                          context,
                              () {
                            if (_formKey.currentState!.validate()) {
                              setState(() {
                                email = emailController.text;
                                password = passwordController.text;
                              });
                              userLogin();
                            }
                          },
                          'Login',
                          "Don't have an account?",
                          'Register!',
                              () {
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                  pageBuilder: (context, a, b) => SignUp(),
                                  transitionDuration: Duration(seconds: 0)),
                            );
                          }),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Divider(
                                color: Colors.black87,
                              )),
                          Text(" or "),
                          Expanded(
                              child: Divider(
                                color: Colors.black87,
                              )),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Spacer(
                              flex: 3,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();

  var email = "";
  var password = "";
  var userName = "";
  bool _isLoading = false;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final userNameController = TextEditingController();

  registration() async {
    try {
      setState(() {
        _isLoading = true;
        email = emailController.text;
        password = passwordController.text;
        userName = userNameController.text;
      });

      String r = DateTime.now().toIso8601String();

      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
        email: email,
        password: password,
      )
          .whenComplete(
              () => ServiceProvider().addUserCloud(userName, email));
      print(userCredential);

      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) => Login()), (route) => false);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('Weak Password');
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            'The password you have entered is to weak!',
            style: TextStyle(color: Colors.red),
          ),
          backgroundColor: Colors.white,
        ));
      } else if (e.code == 'email-already-in-use') {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            'The email address used is already assigned to a user!',
            style: TextStyle(color: Colors.red),
          ),
          backgroundColor: Colors.white,
        ));
        print('This email is already in use!');
      }
    }
  }

  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    userNameController.dispose();
    super.dispose();
  }

  bool isObscure = false;
  int _currentStep = 0;
  @override
  Widget build(BuildContext context) {
    List<Step> _getStep() => [
      Step(
          state: _currentStep > 0 ? StepState.complete : StepState.indexed,
          title: Text('User\nInformation'),
          content: Column(
            children: [
              TextFormField(
                validator: (val) {
                  if (val == null || val.isEmpty) {
                    return 'Please enter your username!';
                  }
                },
                controller: userNameController,
                decoration: InputDecoration(
                  labelText: 'Username?',
                  labelStyle: TextStyle(fontSize: 17, color: Colors.amber),
                  errorStyle: TextStyle(color: Colors.black26),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
          isActive: _currentStep >= 0),
      Step(
          state: _currentStep > 1 ? StepState.complete : StepState.indexed,
          title: Text('Login\nInformation'),
          content: Column(
            children: [
              formEmailTextField(emailController),
              FormPassTextField(passwordController),
            ],
          ),
          isActive: _currentStep >= 1),
      Step(
          state: _currentStep > 2 ? StepState.complete : StepState.indexed,
          title: Text('Information\nPreview'),
          content: Column(
            children: [
              ListTile(
                leading: Icon(Icons.person, color: Colors.amber),
                title: Text(userNameController.text),
              ),
              ListTile(
                leading: Icon(Icons.mail, color: Colors.amber),
                title: Text(emailController.text),
              ),
              ListTile(
                leading: Icon(
                  Icons.password,
                  color: Colors.amber,
                ),
                title:
                Text(isObscure ? passwordController.text : "*********"),
                trailing: IconButton(
                  icon: Icon(
                    Icons.remove_red_eye,
                    color: isObscure ? Colors.amber : Colors.grey[500],
                  ),
                  onPressed: () {
                    setState(() {
                      isObscure = !isObscure;
                    });
                  },
                ),
              )
            ],
          ),
          isActive: _currentStep >= 2),
    ];

    return Scaffold(
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Form(
        key: _formKey,
        child: SingleChildScrollView(
          keyboardDismissBehavior:
          ScrollViewKeyboardDismissBehavior.onDrag,
          child: Column(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.18,
                child: Container(
                  alignment: Alignment.bottomLeft,
                  color: Colors.amber,
                  child: Text(
                    'Register',
                    style: Theme.of(context)
                        .textTheme
                        .headline4!
                        .copyWith(color: Colors.white),
                  ),
                  padding: EdgeInsets.only(left: 20, bottom: 10),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 15),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.82,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Stepper(
                  controlsBuilder: (context,
                      {onStepCancel, onStepContinue}) {
                    return Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Divider(),
                        Row(
                          children: [
                            if (_currentStep != 0)
                              Expanded(
                                  child: TextButton(
                                    onPressed: onStepCancel,
                                    child: Text('Back'),
                                  )),
                            Expanded(
                                child: TextButton(
                                  onPressed: onStepContinue,
                                  child: Text(
                                      _currentStep == 2 ? 'Register' : 'Next'),
                                )),
                          ],
                        ),
                      ],
                    );
                  },
                  steps: _getStep(),
                  type: StepperType.horizontal,
                  currentStep: _currentStep,
                  onStepContinue: () {
                    final isLastStep =
                        _currentStep == _getStep().length - 1;
                    if (isLastStep) {
                      registration();
                    } else {
                      setState(() {
                        _currentStep += 1;
                      });
                    }
                  },
                  onStepCancel: () {
                    if (_currentStep != 0) {
                      setState(() {
                        _currentStep--;
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final _formKey = GlobalKey<FormState>();

  var email = "";

  final emailController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    super.dispose();
  }

  resetPassword() async {
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Password reset email has been sent!')));
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Login()));
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('User not registered!');
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('Email has not regestered!')));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Form(
        key: _formKey,
        child: formContent(
          context,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              formTopTitle(context, 'Forgot my Password!'),
              Container(
                margin: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
                child: formEmailTextField(emailController),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            email = emailController.text;
                          });
                          resetPassword();
                        }
                      },
                      child: Text('Email')),
                  TextButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) => Login()));
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.black),
                      )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Don't have an account yet?"),
                  TextButton(
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            PageRouteBuilder(
                                pageBuilder:
                                    (context, animation1, animation2) =>
                                    SignUp(),
                                transitionDuration: Duration(seconds: 0)),
                                (route) => false);
                      },
                      child: Text(
                        'Register',
                        style: TextStyle(
                          color: Colors.amber,
                        ),
                      )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NewPassword extends StatefulWidget {
  NewPassword({Key? key}) : super(key: key);

  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  final _formKey = GlobalKey<FormState>();
  var newPassword = "";
  final newPasswordTextEditingController = TextEditingController();
  final User currentUser = FirebaseAuth.instance.currentUser!;
  changePassword() async {
    try {
      currentUser.updatePassword(newPassword);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Login()));
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
              'Your password has been updated/changed. Please login using your new password.')));
    } catch (e) {}
  }

  @override
  void dispose() {
    newPasswordTextEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Form(
          key: _formKey,
          child: formContent(
              context,
              Column(
                children: [
                  formTopTitle(context, 'Creating your new password!'),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    child: FormPassTextField(
                      newPasswordTextEditingController,
                    ),
                  ),
                  formActionButton(context, () {
                    if (_formKey.currentState!.validate()) {
                      setState(() {
                        newPassword = newPasswordTextEditingController.text;
                      });
                      changePassword();
                    }
                  }, 'Reset Password', '', '', () {}),
                ],
              ))),
    );
  }
}

class ServiceProvider extends ChangeNotifier {
  UserModel? _loggedInUser;
  UserModel get loggedInUser => _loggedInUser!;

  final CollectionReference collectionRef =
  FirebaseFirestore.instance.collection("users");

  addUserCloud(String userName, String imageUrl) async {
    User? _currentUser = FirebaseAuth.instance.currentUser;

    UserModel userModel = UserModel(
        uid: _currentUser!.uid,
        email: _currentUser.email,
        userName: userName);
    await collectionRef.doc(_currentUser.uid).set(userModel.toMap());
  }

  Future<bool> isUserCloud() async {
    final _checkUser;
    User? _currentUser = FirebaseAuth.instance.currentUser;

    final _result =
    await collectionRef.where("uid", isEqualTo: _currentUser!.uid).get();
    if (_result.docs.isEmpty) {
      _checkUser = true;
    } else {
      _checkUser = false;
    }
    return _checkUser;
  }

  Future getUserData() async {
    User? _currentUser = FirebaseAuth.instance.currentUser;

    try {
      await collectionRef.doc(_currentUser!.uid).get().then((value) {
        _loggedInUser = UserModel.fromMap(value.data());
      });
    } catch (e) {
      print(e);
    }

    notifyListeners();
  }

  Future logout() async {
    await FirebaseAuth.instance.signOut();
  }
}

