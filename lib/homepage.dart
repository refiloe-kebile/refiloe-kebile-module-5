import 'package:flutter/material.dart';
import 'package:flutter_app/login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Refiloe Kebile Module 5 App'),
        actions: [
          IconButton(
            onPressed: () async {
              //  context.read<ServiceProvider>().logout();
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => Profile()));
            },
            icon: Icon(Icons.person),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(child: Text('Welcome to My App')),
          Image.asset('assets/App-Academy.png'),
        ],
      ),
    );
  }
}

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  User? user = FirebaseAuth.instance.currentUser!;

  verifyEmail() async {
    if (user != null && !user!.emailVerified) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Your verification email has been sent!')));
    }
  }

  @override
  Widget build(BuildContext context) {

    final pr = context.watch<ServiceProvider>();

    return Scaffold(
      body: _buildProfile(
          context, pr.loggedInUser.userName!, pr.loggedInUser.imageUrl!),
    );
  }

  DefaultTabController _buildProfile(
      BuildContext context, String userName, String imageUrl) {
    return DefaultTabController(
      length: 2,
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              actions: [
                IconButton(
                    onPressed: () async {
                      await context.read<ServiceProvider>().logout();
                      await Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => Login()),
                      );
                    },
                    icon: Icon(
                      Icons.logout,
                      color: Colors.white,
                    )),
              ],
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(userName,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Container(
                    alignment: Alignment.center,
                    child: CircleAvatar(
                        radius: 45,
                        child: ClipRRect(
                          child: Image.network(
                            imageUrl,
                            fit: BoxFit.cover,
                          ),
                        )),
                  )),
            ),
            SliverPersistentHeader(
              delegate: _SliverAppBarDelegate(
                TabBar(
                  labelColor: Colors.black87,
                  unselectedLabelColor: Colors.grey,
                  tabs: [
                    Tab(text: "Profile"),
                  ],
                ),
              ),
              pinned: true,
            ),
          ];
        },
        body: TabBarView(
          children: [
            Column(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.email,
                    color: Colors.amber,
                  ),
                  title: Text(user!.email!),
                  trailing: IconButton(
                      onPressed: () {
                        verifyEmail();
                      },
                      icon: Icon(
                        Icons.verified,
                        color: user!.emailVerified
                            ? Colors.amber
                            : Colors.grey[200],
                      )),
                ),
                ListTile(
                  leading: Icon(
                    Icons.password,
                    color: Colors.amber,
                  ),
                  title: Text("******"),
                  trailing: IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NewPassword()));
                      },
                      icon: Icon(
                        Icons.change_circle,
                      )),
                ),
              ],
            ),
            Container(
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}

class DesignProvider extends ChangeNotifier {
  Color? _suffixColor = Colors.grey[350];
  Color get suffixColor => _suffixColor!;
  bool _obscureText = true;
  bool get obscureText => _obscureText;

  suffixIconProcess() {
    _obscureText = !_obscureText;
    _obscureText
        ? _suffixColor = Colors.grey[350]
        : _suffixColor = Colors.amber;
    notifyListeners();
  }
}

Widget formActionButton(
    BuildContext context,
    Function elevatedButtonFunction,
    String elevatedButtonText,
    String questionText,
    String textButtonText,
    Function textButtonFunction) {
  return Column(
    children: [
      Center(
        child: ElevatedButton(
          onPressed: () => elevatedButtonFunction(),
          child: Text(elevatedButtonText),
          style: ElevatedButton.styleFrom(
            fixedSize: Size(200.0, 30.0),
            primary: Colors.amber,
          ),
        ),
      ),
      Container(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(questionText),
              TextButton(
                  onPressed: () => textButtonFunction(),
                  child: Text(
                    textButtonText,
                    style: TextStyle(
                      color: Colors.amber,
                    ),
                  )),
            ],
          )),
    ],
  );
}

Widget formContent(BuildContext context, Widget child) {
  return ListView(
    children: [
      SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.19,
        child: Container(
          color: Colors.amber,
        ),
      ),
      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.77,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: child,
      ),
    ],
  );
}

TextFormField formEmailTextField(
    TextEditingController controller,
    ) {
  return TextFormField(
    autofocus: false,
    controller: controller,
    keyboardType: TextInputType.emailAddress,
    autofillHints: [AutofillHints.email],
    decoration: InputDecoration(
      labelText: 'Email',
      labelStyle: TextStyle(fontSize: 17, color: Colors.amber),
      errorStyle: TextStyle(color: Colors.black26),
    ),
    validator: (value) {
      if (value == null || value.isEmpty) {
        return 'Please enter your email address!';
      } else if (!value.contains('@')) {
        return 'Please enter a valid email address!';
      }
      return null;
    },
  );
}

class FormPassTextField extends StatelessWidget {
  final TextEditingController passwordController;
  FormPassTextField(this.passwordController);
  @override
  Widget build(BuildContext context) {
    final pr = context.watch<DesignProvider>();
    return TextFormField(
      controller: passwordController,
      keyboardType: TextInputType.visiblePassword,
      autofillHints: [AutofillHints.password],
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter your password!';
        } else if (value.length < 5) {
          return 'Your password is too short. Please enter a password longer then 6 charaters.';
        }
        return null;
      },
      autofocus: false,
      obscureText: pr.obscureText,
      decoration: InputDecoration(
        suffixIcon: IconButton(
          onPressed: () => pr.suffixIconProcess(),
          icon: Icon(
            Icons.remove_red_eye_sharp,
            color: pr.suffixColor,
          ),
        ),
        labelText: 'Password',
        labelStyle: TextStyle(fontSize: 17, color: Colors.amber),
        errorStyle: TextStyle(color: Colors.black26),
      ),
    );
  }
}

Widget formTopTitle(BuildContext context, String title) {
  return Padding(
    padding: const EdgeInsets.only(left: 15.0, top: 20, bottom: 10),
    child: Text(
      title,
      style: Theme.of(context)
          .textTheme
          .headline4!
          .copyWith(color: Colors.black87),
    ),
  );
}