import 'package:flutter/material.dart';
import 'package:flutter_app/login.dart';
import 'package:flutter_app/homepage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

//import 'dart:math';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ServiceProvider>(create: (context) => ServiceProvider()),
        ChangeNotifierProvider<DesignProvider>(
            create: (context) => DesignProvider()),
      ],
      builder: (context, _) => MaterialApp(
          debugShowMaterialGrid: false,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            scaffoldBackgroundColor: Colors.white,
            primarySwatch: Colors.amber,
          ),
          home: Login()),
    );
  }
}

class UserModel {
  String? uid;
  String? email;
  String? userName;
  String? imageUrl;
  UserModel({this.uid, this.email, this.userName, this.imageUrl});

  factory UserModel.fromMap(map) => UserModel(
      uid: map['uid'],
      email: map['email'],
      userName: map['userName'],
      imageUrl: map['imageUrl']);

  Map<String, dynamic> toMap() => {
    'uid': uid,
    'email': email,
    'userName': userName,
    'imageUrl': imageUrl,
  };
}

